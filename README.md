# Testing and System Testing and TDD

This repo is going to be a TDD exercise for fizzbuzz, while we discuss testing and system testing. 


## Testing & System Testing

Why testting?
- Helps us know when we are done (& the client)
- Makes code more maintainable
- Helps reduce technical debt
- Makes sure system are working as we change them &/or update
- Importance for complience (usually it's external testing teams)

Teams on a Development of a Product: 
- Dev Team
- The DevOps (ops team)
- Testers 
- Business / Product Team

## What type to types of test: 
- Black box Testing
  - Don't know how the code is running & don't care
  - test the system as a whole
  - Tools: selenium - Opens up a web browser and clicks and writes stuff
    

- White box Testing 
 - Know the ins and out's of code
 - Test individual sections 
 - More like Unit testing


## Levels of testing
- Unit testing (single functions and classes/methos)
- Integration testing (groups of functions and classes methods together)
- System testing (Functions and classes working on a system (a server in port 80))
- Acceptance testing (human who is paying saying it's good)

![Testing V Model](https://www.guru99.com/images/6-2015/052715_0904_GuidetoSDLC3.png)

## Importance of testing for DevOps and Agile

**In terms of agile, it just ensures working code.**

**In term of DevOps, you are automating code to be deployed, therefore tests will ensure you're team is automating good working code** 

We all make mistakes, some can be more costly than others. If they end up in production they are usually even more expensive. 

## TDD

Write a test. It fails. write the least amount of code until it passes.
Re fractor / make more tests, Make it fail. And fix it. 

![TDD](https://marsner.com/wp-content/uploads/test-driven-development-TDD.png) 



## FizzBuzz Exercise

This exercise is a classic, like hello world, in the programming community. 

We we'll do this using User stories, Acceptance criterea and DoF.
We'll also do TDD. to ensure Agile way of working 

## Difference between acceptance criteria and Definition of Done 
-- acceptance criteria --> is unit to one Card / User story 
-- Definition of Done --> applies to EVERY CARD / User Story 

Definition of Done is the duedilligence and emta work around completing a task.
Defined with product owner. 

Things like (meta work): 
- every user story should abe a seperate branch on git
- Every user story should be documented
- Once the User story in review, need to be emailed to mike@google.com 
- Clap 3 times when done for good luck 
- Things that are important as team
- If doing SQL structural changes - update schema and inform scrum master and lead DeV before anything is pushed to master 


### Main user stories and Epics

[Epic] As a user I want a program that runs Fizzbuzz, up to any number should output a list and should be tested, so that I know it works.

[User story]
As a user when the program gets a multiple of 3, it should print out fizz.

[User story]
As a user when the program gets a multiple of 5, it should print out buzz.

[User story]
As a user when the program gets a multiple of 3 and 5, it should print out fizzbuzz.

[User story]
As a user when I give program a number, it should play fizzbuzz up to said number.

**Trello link:** https://trello.com/invite/b/g7FtfPFJ/704f190e18a8d8609ede890dc20f9e3b/cohort7-fizzbuzz-tdd


