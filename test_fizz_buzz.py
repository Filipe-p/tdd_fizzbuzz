import pytest
import fizzbuzz_functions
## Test for Fizz
def test_assert_fizz_with_3():
    assert fizzbuzz_functions.check_if_multiple_3(3) == 'fizz'

def test_assert_fizz_with_2():
    assert fizzbuzz_functions.check_if_multiple_3(2) == '2'

def test_assert_fizz_outcome_str():
    assert type(fizzbuzz_functions.check_if_multiple_3(2)) is str

## Tests for buzz
def test_assert_buzz_with_5():
    assert fizzbuzz_functions.check_if_multiple_5(5) == 'buzz'

def test_assert_buzz_with_2():
    assert fizzbuzz_functions.check_if_multiple_5(2) == '2'

def test_assert_buzz_outcome_str():
    assert type(fizzbuzz_functions.check_if_multiple_5(2)) is str

### Test for fizzbuzz

# Write a test for a function call check_if_multiple_of_3_5()
## check if it prints fizzbuzz
## if it prints a str(number) otherwise
## Check that it outputs a STR.

def test_assert_fizzbuzz_with_5():
    assert fizzbuzz_functions.check_if_multiple_15(5) == '5'

def test_assert_fizzbuzz_with_2():
    assert fizzbuzz_functions.check_if_multiple_15(2) == '2'

def test_assert_fizzbuzz_with_15():
    assert fizzbuzz_functions.check_if_multiple_15(2) == 'fizzbuzz'

def test_assert_fizzbuzz_with_30():
    assert fizzbuzz_functions.check_if_multiple_15(2) == 'fizzbuzz'

def test_assert_fizzbuzz_outcome_str():
    assert type(fizzbuzz_functions.check_if_multiple_15(2)) is str

## Check integration test
## Make a test on run_fizz_buzz functions
## should output list
# go check acceptance criteria for more details on tests

def test_assert_run_fizz_buzz_with_5():
    assert fizzbuzz_functions.run_fizz_buzz(5) == ['1', '2', 'fizz', '4', 'buzz']

def test_assert_run_fizz_buzz_with_15():
    assert fizzbuzz_functions.run_fizz_buzz(5) == ['1', '2', 'fizz', '4', 'buzz', 'fizz', '7', '8', 'fizz', 'buzz', '11', 'fizz', '14', 'fizzbuzz' ]

def test_assert_assert_run_fizz_buzz_outcome_str():
    assert type(fizzbuzz_functions.run_fizz_buzz(2)) is list