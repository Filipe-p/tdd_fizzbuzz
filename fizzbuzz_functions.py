def check_if_multiple_3(num_arg):
    if num_arg % 3 == 0:
        return 'fizz'
    else:
        return str(num_arg)

if __name__ == '__main__':

    # Tests have 3 parts
    ## Known inputs
    ## Known  blocks of code (functions, method)
    ## Expected outputs
    ##and you assert them

    print('////Test 1')
    tdd_input = 3
    tdd_expect_outcome = 'fizz'
    print(f" Testing check_if_multiple_3() with {tdd_input}, expected: {tdd_expect_outcome}")
    print(f" got: {check_if_multiple_3(tdd_input)}")
    print("Test Result:", check_if_multiple_3(tdd_input) == tdd_expect_outcome)

    print('////Test 2')
    tdd_input = 2
    tdd_expect_outcome = '2'
    print(f" Testing check_if_multiple_3() with {tdd_input}, expected: {tdd_expect_outcome}")
    print(f" got: {check_if_multiple_3(tdd_input)}")
    print("Test Result:", check_if_multiple_3(tdd_input) == '2')


    #print('////Test 3')
    #tdd_input = 2
    #tdd_expect_outcome = type('2')
    #print(f" Testing check_if_multiple_3() with {tdd_input}, expected data type to be: {tdd_expect_outcome}")
    #print(f" got: {check_if_multiple_3(tdd_input)}")
    #print("Test Result:", check_if_multiple_3(tdd_input) == tdd_expect_outcome)
